#include <stdio.h>
#define N	51

void fill(int f[], int n) {
	f[0] = 0; f[1] = 1;
	int i;
	for (i = 2; i <= n; i++)
		f[i] = f[i - 1] + f[i - 2];
}

void print(int f[], int n) {
	int i;
	for (i = 0; i <= n; i++)
		printf("%d ", f[i]);
	putchar('\n');
}

int main(void) {
	static int f[N];
	int n;
	scanf("%d", &n);
	fill(f, n);
	print(f, n);
	return 0;
}

