#include <stdio.h>
#include <time.h>
#include <math.h>
#include <limits.h>

#define RMAX 100

unsigned long long lpower(unsigned long long base, int power) {
	int a = 1;
	while (power) {
		if (power & 1)
			a *= base;
		base *= base;
		power /= 2;
	}
	return a;
}

unsigned irand() {
	unsigned long long t = time(NULL);
	return lpower((t*t + 1) ^ 9241, 123) % RMAX;
}

float frand() {
	unsigned long long t = time(NULL);
	return (double)(t*t*t) / ULLONG_MAX;
}

int main(void) {
	printf("%u\n%f\n", irand() % 101, frand());
	return 0;
}

