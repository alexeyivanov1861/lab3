#include <stdio.h>
#include <math.h>

double pos_power(double base, int p) {
	if (p == 0)
		return 1;
	if (p % 2)
		return base * pos_power(base, p - 1);
	else
		return pos_power(base * base, p / 2);
}

double rpower(float base, int p) {
	if (p > 0)
		return pos_power(base, p);
	return 1.0 / pos_power(base, -p);
}

double power(double base, int p) {
	double a = 1.0;
	int neg = 0;
	if (p < 0) {
		p = -p;
		neg = 1;
	}
	while (p) {
		if (p & 1) //power not even
			a *= base;
		base *= base;
		p /= 2;
	}
	return neg ? 1.0 / a : a;
}

int main(void) {
	double base;
	int p;
	scanf("%lf %d", &base, &p);
	printf("My non rec  power: %lf\n", power(base ,p));
	printf("My rec      power: %lf\n", rpower(base ,p));
	printf("Standart    power: %lf\n", pow(base, p));
	return 0;
}
