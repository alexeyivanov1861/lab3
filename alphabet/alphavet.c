#include <stdio.h>

int main(void) {
	int i;
	printf("\\n %d\n", '\n');
	printf("' ' %d\n", ' ');
	for (i = 'A'; i <= 'Z'; i++)
		printf("%c %d\n", i, i);
	for (i = 'a'; i <= 'z'; i++)
		printf("%c %d\n", i, i);
	return 0;
}
